import './App.css';
import DemoJSS from './JSS_Styled-Components/DemoJSS/DemoJSS';
import DemoTheme from './JSS_Styled-Components/Themes/DemoTheme';
import TodoList from './JSS_Styled-Components/BaiTapStyleComponent/TodoList/TodoList';
import LifeCycleReact from './LifeCycleReact/LifeCycleReact';


function App() {
  return (
    <div >
      {/* <DemoJSS /> */}
      {/* <DemoTheme /> */}
      <TodoList />
      {/* <LifeCycleReact /> */}
    </div>
  );
}

export default App;
