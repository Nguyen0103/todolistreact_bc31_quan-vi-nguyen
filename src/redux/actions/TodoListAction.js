import { ADD_TASK, CHANGE_THEME, DELETE_TASK, DONE_TASK, EDIT_TASK, UPDATE_TASK } from "../types/TodoListType";

export const addTaskAction = (newTask) => {
    return ({
        type: ADD_TASK,
        newTask,
    })
}

export const changeThemeAction = (themeId) => {
    return ({
        type: CHANGE_THEME,
        themeId,
    })
}

export const doneTaskAction = (taskId) => ({
    type: DONE_TASK,
    taskId,
})
// {return {}} = ({});

export const deleteTaskAction = (taskId) => ({
    type: DELETE_TASK,
    taskId,
})

export const editTaskACtion = (task) => ({
    type: EDIT_TASK,
    task,
})

export const updateTaskAction = (taskName) => ({
    type: UPDATE_TASK,
    taskName,
})