// Theme
import { arrTheme } from "../../JSS_Styled-Components/Themes/ThemeManager"
import { TodoListDarkTheme } from "../../JSS_Styled-Components/Themes/TodoListDarkTheme"
import { TodoListLightTheme } from "../../JSS_Styled-Components/Themes/TodoListLightTheme"
import { TodoListPrimaryTheme } from "../../JSS_Styled-Components/Themes/TodoListPrimaryTheme"

// Types
import { ADD_TASK, CHANGE_THEME, DELETE_TASK, DONE_TASK, EDIT_TASK, UPDATE_TASK } from "../types/TodoListType"

const initialState = {
    themeTodoList: TodoListPrimaryTheme,
    taskList: [
        { id: 'task-1', taskName: 'task 1', done: true, },
        { id: 'task-2', taskName: 'task 2', done: false, },
        { id: 'task-3', taskName: 'task 3', done: true, },
        { id: 'task-4', taskName: 'task 4', done: false, },
    ],
    taskEdit: { id: '-1', taskName: '', done: false, },

}

export default (state = initialState, action) => {
    switch (action.type) {
        case ADD_TASK: {
            //check empty
            if (action.newTask.taskName.trim() === '') {
                alert('task name is required')
                return ({ ...state })
            }
            // check exists
            let taskListUpdate = [...state.taskList];

            let index = taskListUpdate.findIndex(task => task.taskname === action.newTask.taskName);

            if (index !== -1) {
                alert('task name already exists !')
                return ({ ...state })
            }

            taskListUpdate.push(action.newTask);

            // end progress and update taskList in state
            state.taskList = taskListUpdate;
            return ({ ...state })
        }

        case CHANGE_THEME: {
            let theme = arrTheme.find(theme => theme.id == action.themeId)
            if (theme) {
                state.themeTodoList = { ...theme.theme };
            }

            return ({ ...state })
        }

        case DONE_TASK: {
            // click vào button check => dispatch lên action có taskId
            let taskListUpdate = [...state.taskList]
            // từ taskId tìm ra task đó ở vị trí nào trong mảng tiến hành update lại thuộc tính done='true', update lại state redux
            let index = taskListUpdate.findIndex(task => task.id === action.taskId)

            if (index !== -1) {
                taskListUpdate[index].done = true
            }

            // state.taskList = taskListUpdate;
            return ({ ...state, taskList: taskListUpdate })
        }

        case DELETE_TASK: {
            let taskListUpdate = [...state.taskList]
            // cách 1
            // let index = taskListUpdate.findIndex(task => task.id === action.taskId)
            // if (index !== -1) {
            //     taskListUpdate.splice(index, 1)
            // }

            // cách 2
            //gán lại giá trị cho mảng taskListUpdate = chính nó nhưng filter lại không có taskId đó.
            taskListUpdate = taskListUpdate.filter(task => task.id !== action.taskId)

            return ({ ...state, taskList: taskListUpdate })
        }

        case EDIT_TASK: {
            return ({ ...state, taskEdit: action.task })
        }

        case UPDATE_TASK: {
            //edit lại taskName của taskEdit
            state.taskEdit = { ...state.taskEdit, taskName: action.taskName }

            //tìm trong taskList
            let taskListUpdate = [...state.taskList]

            let index = taskListUpdate.findIndex(task =>
                task.id === state.taskEdit.id
            )

            if (index !== -1) {
                taskListUpdate[index] = state.taskEdit;
            }

            state.taskEdit = { id: '-1', taksName: '', done: false }


            return ({ ...state, taskList: taskListUpdate })
        }

        default:
            return ({ ...state })
    }
}
