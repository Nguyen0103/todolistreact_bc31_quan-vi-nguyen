import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ThemeProvider } from "styled-components";

// Layout Component
import { Container } from '../../TodoListComponent/Container';
import { Heading1, Heading2, Heading3, Heading4, Heading5 } from '../../TodoListComponent/Heading';
import { Dropdown } from '../../TodoListComponent/Dropdown';
import { Label, TextField, Input } from '../../TodoListComponent/TextField';
import { Button } from '../../TodoListComponent/Button';
import { Table, Thead, Tbody, Tr, Td, Th } from '../../TodoListComponent/Table';
import { arrTheme } from '../../Themes/ThemeManager';

// Theme Component
import { TodoListDarkTheme } from '../../Themes/TodoListDarkTheme';
import { TodoListLightTheme } from '../../Themes/TodoListLightTheme';
import { TodoListPrimaryTheme } from '../../Themes/TodoListPrimaryTheme';

import { addTaskAction, changeThemeAction, deleteTaskAction, doneTaskAction, editTaskACtion, updateTaskAction } from '../../../redux/actions/TodoListAction';

class TodoList extends Component {

    state = {
        taskName: '',
        disabled: true,
    }

    renderTaskListToDo = () => {
        return this.props.taskList.filter(task => task.done === false).map((task, index) => {
            return (
                <Tr
                    key={index}
                    style={{ heigh: '100' }}
                >
                    <Th style={{ verticalAlign: 'middle' }}>{task.taskName}</Th>
                    <Th className='text-right' >
                        <Button
                            onClick={() => {
                                this.setState({
                                    disabled: false,
                                }, () => {
                                    this.props.dispatch(editTaskACtion(task))
                                })
                            }}
                        ><i className="fa fa-edit"></i></Button>

                        <Button
                            onClick={() => {
                                this.props.dispatch(deleteTaskAction(task.id))
                            }}
                        ><i className="fa fa-trash"></i></Button>

                        <Button
                            onClick={() => {
                                this.props.dispatch(doneTaskAction(task.id))
                            }}
                        ><i className="fa fa-check"></i></Button>
                    </Th>
                </Tr>
            )
        })
    }

    renderTaskListCompleted = () => {
        return this.props.taskList.filter(task => task.done === true).map((task, index) => {
            return (
                <Tr
                    key={index}
                    style={{ heigh: '100' }}
                >
                    <Th style={{ verticalAlign: 'middle' }}>{task.taskName}</Th>
                    <Th className='text-right' >
                        <Button
                            onClick={() => {
                                this.props.dispatch(deleteTaskAction(task.id))
                            }}
                        ><i className="fa fa-trash"></i></Button>
                    </Th>
                </Tr>
            )
        })
    }

    renderTheme() {
        return arrTheme.map((theme, index) => {
            return (<option value={theme.id} style={{ textAlign: "center" }}>{theme.name}</option>)
        })
    }

    // componentWillReceiveProps(newProps) {
    //     this.setState({
    //         taskName: newProps.taskEdit.taskName
    //     })
    // }


    // lifeCycle tĩnh không thể truy cập con trỏ this.
    // static getDerivedStateFromProps(newProps, currentState) {
    //     newProps: là props mớ, props cũ là this.props(không thể truy xuất)

    //     currentState: ứng với state hiện tại this.state

    //     hoặc trả về state mới(this.state)
    //     let newState = { ...currentState, taskName: newProps.taskEdit.taskName }
    //     return newState;

    //     trả về null
    //     return null;
    // }

    render() {
        return (
            <ThemeProvider theme={this.props.themeTodoList}>
                <Container className='w-50 mt-5'>
                    <Dropdown
                        onChange={(e) => {
                            let { value } = e.target;
                            this.props.dispatch(changeThemeAction(value))
                        }}
                        className='mb-5'
                    >
                        {this.renderTheme()}
                    </Dropdown>
                    <Heading3 className='mt-5'>TO DO LIST</Heading3>
                    <TextField
                        value={this.state.taskName}
                        onChange={(e) => {
                            this.setState({
                                taskName: e.target.value
                            })
                        }}
                        name='taskName'
                        label='Task Name'
                        className='w-50'
                    />

                    {this.state.disabled

                        ? (<Button
                            onClick={() => {
                                // lấy thông tin user input 
                                let { taskName } = this.state;
                                // tạo ra 1 obj 
                                let newTask = {
                                    id: Date.now(),
                                    taskName: taskName,
                                    done: false
                                }
                                // đưa task obj lên redux => thông qua 'dispatch'
                                this.props.dispatch(addTaskAction(newTask))
                            }}
                            className='ml-2'>
                            <i className='fa fa-plus mr-1'></i>
                            Add Task
                        </Button>)
                        : (<Button
                            disabled
                            onClick={() => {
                                // lấy thông tin user input 
                                let { taskName } = this.state;
                                // tạo ra 1 obj 
                                let newTask = {
                                    id: Date.now(),
                                    taskName: taskName,
                                    done: false
                                }
                                // đưa task obj lên redux => thông qua 'dispatch'
                                this.props.dispatch(addTaskAction(newTask))
                            }}
                            className='ml-2'>
                            <i className='fa fa-plus mr-1'></i>
                            Add Task
                        </Button>)}

                    {this.state.disabled

                        ? (<Button
                            disabled
                            onClick={() => {
                                this.props.dispatch(updateTaskAction(this.state.taskName))
                            }}
                            className='ml-2 '>
                            <i className="fa fa-upload mr-1"></i>
                            Update Task
                        </Button>)

                        : (<Button
                            onClick={() => {
                                let { taskName } = this.state;

                                this.setState({
                                    disabled: true,
                                    taskName: ''
                                }, () => {
                                    this.props.dispatch(updateTaskAction(taskName))
                                })
                            }}
                            className='ml-2'>
                            <i className="fa fa-upload mr-1"></i>
                            Update Task
                        </Button>)
                    }

                    <hr />

                    <Heading3>Task To Do</Heading3>
                    <Table>
                        <Thead>
                            {this.renderTaskListToDo()}
                        </Thead>
                    </Table>
                    <Heading3>Task Completed</Heading3>
                    <Table>
                        <Thead>
                            {this.renderTaskListCompleted()}
                        </Thead>
                    </Table>

                </Container>
            </ThemeProvider >
        )
    }

    //lifecycle: LC

    //Đây là lifeCycle => OldProps & OldState của Component trước khi render(LC này chạy sau render)
    componentDidUpdate(prevProps, prevState) {
        //so sánh nếu như props trước đó (taskEdit trước mà khác với taskEdit hiện tại => setState )
        if (prevProps.taskEdit.id !== this.props.taskEdit.id) {
            this.setState({
                taskName: this.props.taskEdit.taskName,
            })
        }
    }
}

let mapStateToProps = (state) => {
    return ({
        themeTodoList: state.TodoListReducer.themeTodoList,
        taskList: state.TodoListReducer.taskList,
        taskEdit: state.TodoListReducer.taskEdit,
    })
}

export default connect(mapStateToProps)(TodoList);