export const TodoListLightTheme = {
    bgColor: '#fff',
    InputTextColor: '#7952b3',
    color: '#7952b3',
    borderButton: '1px solid #7952b3',
    borderRadiusButton: 'none',
    hoverTextColor: '#fff',
    hoverBgColor: '#7952b3',
    borderColor: '#7952b3',
}

