export const TodoListPrimaryTheme = {
    bgColor: '#fff',
    InputTextColor: '#343a40',
    color: '#343a40',
    borderButton: '1px solid #343a40',
    borderRadiusButton: 'none',
    hoverTextColor: '#fff',
    hoverBgColor: '#343a40',
    borderColor: '#343a40',
}


