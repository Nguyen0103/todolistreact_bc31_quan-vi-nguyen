import { TodoListDarkTheme } from "./TodoListDarkTheme"
import { TodoListLightTheme } from "./TodoListLightTheme"
import { TodoListPrimaryTheme } from "./TodoListPrimaryTheme"

export const arrTheme = [
    { id: 3, name: 'Primary', theme: TodoListPrimaryTheme },
    { id: 1, name: 'Dark', theme: TodoListDarkTheme },
    { id: 2, name: 'Light', theme: TodoListLightTheme },
]