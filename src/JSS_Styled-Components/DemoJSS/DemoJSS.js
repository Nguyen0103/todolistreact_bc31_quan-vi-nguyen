import React, { Component } from 'react';
import { Button, SmallButton } from '../components/Button'
import { StyledLink } from '../components/Links';
import { TextField } from '../components/TextField';



export default class DemoJSS extends Component {
    render() {
        return (
            <div>
                <Button
                    className='button-style'
                    bgPrimary
                    fontSize2x
                >
                    Styled-Components
                </Button>

                <SmallButton>Extending Styles</SmallButton>

                <StyledLink id="abc" name='abc123'>
                    Styling any components
                </StyledLink>

                <TextField inputColor="Green" />
            </div>
        )
    }
}
