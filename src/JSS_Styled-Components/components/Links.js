import React from 'react';
import styled from 'styled-components';

// #########two coding style is same same
// export const Link1 = ({ className, children, ...restProps }) => {
//     return (
//         <a className={className}>
//             {children}
//         </a>
//     )
// }

// let { className, children } = props;


//styling any components
export const Link = ({ className, children, ...restProps }) => (
    <a className={className} {...restProps}>
        {children}
    </a>
)

export const StyledLink = styled(Link)`
   color: palevioletred;
   font-weight: blold; 
   background-color: black;
   border-radius: .5rem;
`