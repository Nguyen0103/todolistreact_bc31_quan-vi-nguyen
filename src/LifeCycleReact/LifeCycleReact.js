import React, { Component } from 'react'
import ChildrenComponent from './ChildrenComponent'

export default class LifeCycleReact extends Component {

    constructor(props) {
        super(props)
        this.state = {
            number: 1,
        }
        console.log('constructor');
    }

    //đc gọi khi component này đc sử dụng trên DOM(giao diện của app)
    static getDerivedStateFromProps(newProps, currentState) {
        console.log('getDerivedStateFormProps')
        return null;
    }


    // return true thì chạy tiếp các lifecycle còn lại, ngược lại retrun false sẽ dừng lại không chạy các lifecycle khác.
    shouldComponentUpdate(newProps, newState) {
        return true;
    }

    render() {
        return (
            <div>
                <h1>Parent Components</h1>
                <ChildrenComponent />
                <span>{this.state.number}</span>
                <button className='btn btn-success'
                    onClick={() => {
                        this.setState({
                            number: this.state.number + 1,
                        })
                    }}
                >+</button>
            </div>
        )
    }

    // được gọi sau khi render và chỉ chạy 1 lần duy nhất (trạng thái mounting)
    componentDidMount() {
        console.log('componentDidMount')
    }

    // lần đầu sẽ không gọi, chỉ gọi khi setState hoặc thay đổi Props 
    componentDidUpdate(prevProps, prevState) {
        console.log('componentDidUpdate')
    }


}
